import { atom } from "jotai";
import { atomWithStorage } from "jotai/utils";

export const scoringNameOne = [
  "Tidak Lengkap",
  "Kurang Lengkap",
  "Cukup Lengkap",
  "Lengkap",
  "Sangat Lengkap",
];
export const scoringNameTwo = [
  "Tidak Jelas",
  "Kurang Jelas",
  "Cukup Jelas",
  "Jelas",
  "Sangat Jelas",
];

export const emailAtom = atomWithStorage("email", "");
export const moduleListAtom = atomWithStorage("moduleList", [
  {
    id: "001",
    name: "Sales and Distribution",
  },
  {
    id: "002",
    name: "Financial Accounting",
  },
  {
    id: "003",
    name: "Human Capital Management",
  },
]);

export const moduleAnswerTemp = atom([]);

export const moduleCriteriaAtom = atomWithStorage("moduleCriteria", [
  {
    id: "001",
    criteria: [
      {
        id: 1,
        name: "Apakah materi pelatihan SD sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.4,
      },
      {
        id: 2,
        name: "Apakah kualitas modul SD sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.3,
      },
      {
        id: 3,
        name: "Apakah fasilitas pelatihan sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.1,
      },
      {
        id: 4,
        name: "Apakah kualitas pelatih dalam melakukan pelatihan SD sudah jelas?",
        scoringName: scoringNameTwo,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.2,
      },
    ],
  },
  {
    id: "002",
    criteria: [
      {
        id: 1,
        name: "Apakah materi pelatihan FI sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.4,
      },
      {
        id: 2,
        name: "Apakah kualitas modul FI sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.3,
      },
      {
        id: 3,
        name: "Apakah fasilitas pelatihan sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.1,
      },
      {
        id: 4,
        name: "Apakah kualitas pelatih dalam melakukan pelatihan FI sudah jelas?",
        scoringName: scoringNameTwo,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.2,
      },
    ],
  },
  {
    id: "003",
    criteria: [
      {
        id: 1,
        name: "Apakah materi pelatihan HCM sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.4,
      },
      {
        id: 2,
        name: "Apakah kualitas modul HCM sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.3,
      },
      {
        id: 3,
        name: "Apakah fasilitas pelatihan sudah lengkap?",
        scoringName: scoringNameOne,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.1,
      },
      {
        id: 4,
        name: "Apakah kualitas pelatih dalam melakukan pelatihan HCM sudah jelas?",
        scoringName: scoringNameTwo,
        scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
        weight: 0.2,
      },
    ],
  },
]);

export const trainingModuleResultAtom = atomWithStorage(
  "trainingModuleResult",
  []
);
