export const jsonToParams = (data) => {
  return Object.entries(data)
    .map((e) => e.join("="))
    .join("&");
};
