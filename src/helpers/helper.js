import { useAtomValue } from "jotai/utils";
import { moduleListAtom } from "../atoms/atoms";

export const getModuleNameById = (id) => {
  const moduleList = useAtomValue(moduleListAtom);
  const match = moduleList.filter((v) => v.id === id);
  return match.length > 0 ? match[0].name : "";
};

const getMax = (array) => {
  const numbers = array.map((v) => parseFloat(v.weight));
  return Math.max.apply(Math, numbers);
};

export const calculateResult = (moduleList, criteria, data) => {
  const modules = [];
  const criterias = [];
  const matrix = [];
  const result = [];

  if (data.length > 0) {
    data.map((v) => {
      if (modules.indexOf(v.moduleId) === -1) {
        modules.push(v.moduleId);
      }
    });

    data.map((v) => {
      if (criterias.indexOf(v.criteria) === -1) {
        criterias.push(v.criteria);
      }
    });

    //create matrix
    criterias.map((v, i) => {
      matrix.push([]);
      modules.map((m, index) => {
        const matrixValue = data.filter(
          (d) => d.moduleId === m && d.criteria === v
        )[0];
        matrix[i].push({ c: v, m, weight: matrixValue.weight });
        if (index === modules.length - 1) {
          matrix[i] = {
            max: getMax(matrix[i]),
            weights: matrix[i],
          };
        }
      });
    });

    //generate average
    const average = modules.map((m) => []);
    matrix.map((m) => {
      m.weights.map((w, i) => {
        w.weight = w.weight / m.max;
        average[i].push(w);
      });
    });

    average.map((r, index) => {
      let sum = 0;
      r.map((v, i) => {
        const criteriaWeight = criteria
          .map((c) => {
            return c.criteria.filter((e) => {
              return v.c + 1 === e.id && v.m === c.id;
            });
          })
          .filter((f) => typeof f !== "undefined" && f.length > 0);

        sum += criteriaWeight[0][0].weight * v.weight;

        if (i + 1 === r.length) {
          result.push({ weight: sum, module: r[index].m });
        }
      });
    });

    //find lowest rank
    let lowest = {};
    result.map((v, i) => {
      if (i === 0) {
        lowest = v;
      } else {
        if (v.weight < lowest.weight) {
          lowest = v;
        }
      }
    });

    const lowestModule = moduleList.find((v) => {
      if (v.id === lowest.module) {
        return v.id;
      }
    });

    return lowestModule.name;
  }
};
