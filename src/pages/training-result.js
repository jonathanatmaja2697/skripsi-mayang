import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import React, { useEffect, useState } from "react";

import { Layout } from "../components/layout";
import { Logout } from "../components/logout";
import { trainingModuleResultAtom } from "../atoms/atoms";
import { useAtomValue } from "jotai/utils";

const TrainingResult = () => {
  useEffect(() => {
    console.log(trainingModuleResult);

    if (typeof window !== "undefined") {
      setTableWidth(window.innerWidth - (window.innerWidth * 30) / 100);
    }
  }, []);
  const trainingModuleResult = useAtomValue(trainingModuleResultAtom);
  const [tableWidth, setTableWidth] = useState(500);

  return (
    <Layout>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        <Logout />
        <div style={{ flex: 20, padding: "1rem" }}>
          <h2>Hasil pengolahan</h2>
          <TableContainer
            component={Paper}
            style={{
              width: tableWidth,
              display: "block",
              marginRight: "auto",
              marginLeft: "auto",
            }}
          >
            <Table style={{ width: tableWidth }}>
              <TableHead>
                <TableRow style={{ flexGrow: 1 }}>
                  <TableCell style={{ width: "20%" }}>
                    <b>No</b>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <b>Email</b>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <b>Modul yang dibutuhkan</b>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <b>Batch</b>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <b>Tanggal</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              {trainingModuleResult.length > 0 && (
                <TableBody>
                  {trainingModuleResult.map((v, i) => {
                    return (
                      <TableRow style={{ flexGrow: 1 }} key={i}>
                        <TableCell style={{ width: "20%" }} key={i + "no"}>
                          {i + 1}
                        </TableCell>
                        <TableCell style={{ width: "20%" }} key={i + "name"}>
                          {v.email}
                        </TableCell>
                        <TableCell style={{ width: "20%" }} key={i + "action"}>
                          {v.module}
                        </TableCell>
                        <TableCell style={{ width: "20%" }} key={i + "action"}>
                          {v.batch}
                        </TableCell>
                        <TableCell
                          style={{ width: "20  %" }}
                          key={i + "action"}
                        >
                          {v.date}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </div>
      </div>
    </Layout>
  );
};

export default TrainingResult;
