import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { Delete, Edit } from "react-feather";
import React, { useEffect, useState } from "react";
import { emailAtom, moduleCriteriaAtom, moduleListAtom } from "../atoms/atoms";

import { CustomDialog } from "../components/custom-dialog";
import { Layout } from "../components/layout";
import { Logout } from "../components/logout";
import { useAtom } from "jotai";
import { useAtomValue } from "jotai/utils";
import { useRouter } from "next/router";

const ModuleList = () => {
  const [tableWidth, setTableWidth] = useState(300);
  const email = useAtomValue(emailAtom);
  const [moduleList, setModuleList] = useAtom(moduleListAtom);
  const [criteriaList, setCriteriaList] = useAtom(moduleCriteriaAtom);
  const router = useRouter();
  const [open, setOpen] = useState(false);
  const [id, setId] = useState("");

  const onDelete = (receivedId) => {
    setOpen(true);
    setId(receivedId);
  };

  const processDelete = () => {
    setOpen(false);
    setModuleList(moduleList.filter((v) => v.id !== id));
    setCriteriaList(criteriaList.filter((v) => v.id !== id));
    router.reload();
  };

  const onClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      setTableWidth(window.innerWidth - (window.innerWidth * 30) / 100);
    }
  }, []);

  return (
    <Layout>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "flex-end",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        <Logout />
        {email === "admin@gmail.com" ? (
          <>
            <CustomDialog
              dialogContent="Hapus Modul?"
              open={open}
              onClose={onClose}
              onAgree={processDelete}
            />
            <div
              style={{
                display: "flex",
                flex: 20,
                justifyContent: "flex-start",
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              <h1>List Modul</h1>
              <div style={{ marginBottom: "5rem" }}>
                <TableContainer component={Paper} style={{ width: tableWidth }}>
                  <Table style={{ width: tableWidth }}>
                    <TableHead>
                      <TableRow style={{ flexGrow: 1 }}>
                        <TableCell style={{ width: "25%" }}>
                          <b>No</b>
                        </TableCell>
                        <TableCell style={{ width: "25%" }}>
                          <b>ID Modul</b>
                        </TableCell>
                        <TableCell style={{ width: "25%" }}>
                          <b>Nama Modul</b>
                        </TableCell>
                        <TableCell
                          style={{ width: "25%", textAlign: "center" }}
                        >
                          <b>Action</b>
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    {moduleList.length > 0 && (
                      <TableBody>
                        {moduleList.map((v, i) => {
                          return (
                            <TableRow style={{ flexGrow: 1 }} key={i}>
                              <TableCell
                                style={{ width: "25%" }}
                                key={i + "no"}
                              >
                                {i + 1}
                              </TableCell>
                              <TableCell
                                style={{ width: "25%" }}
                                key={i + "id"}
                              >
                                {v.id}
                              </TableCell>
                              <TableCell
                                style={{ width: "25%" }}
                                key={i + "name"}
                              >
                                {v.name}
                              </TableCell>
                              <TableCell
                                style={{
                                  width: "25%",
                                  textAlign: "center",
                                }}
                                key={i + "action"}
                              >
                                <Edit
                                  color="blue"
                                  style={{
                                    marginRight: "1rem",
                                    cursor: "pointer",
                                  }}
                                  onClick={() => {
                                    router.push(`/module/edit/${v.id}`);
                                  }}
                                />
                                <Delete
                                  color="red"
                                  onClick={() => {
                                    onDelete(v.id);
                                  }}
                                  style={{ cursor: "pointer" }}
                                />
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      </TableBody>
                    )}
                  </Table>
                </TableContainer>
                <Button
                  variant="contained"
                  style={{
                    textTransform: "none",
                    marginLeft: "auto",
                    display: "block",
                    marginTop: "2rem",
                  }}
                  onClick={() => {
                    router.push("/module/create");
                  }}
                >
                  Tambah Modul
                </Button>
              </div>
            </div>
          </>
        ) : (
          <div
            style={{
              flex: 15,
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
            }}
          >
            No Access
          </div>
        )}
      </div>
    </Layout>
  );
};

export default ModuleList;
