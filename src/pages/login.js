import { Button, TextField } from "@mui/material";
import { useAtom } from "jotai";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { emailAtom } from "../atoms/atoms";
import { Layout } from "../components/layout";


const Login = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailState, setEmailAtom] = useAtom(emailAtom);
  const [render, setRender] = useState(false);

  useEffect(() => {
    if (emailState !== "" && typeof emailState !== "undefined") {
      router.push("/home");
    } else {
      setRender(true);
    }
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      setEmailAtom(email);
      router.push("/home");
    } catch (err) {
      alert(err);
    }
  };

  return (
    <Layout centered>
      {render && (
        <>
          <Head>
            <title>SAW / Login</title>
          </Head>
          <div>
            <form
              onSubmit={onSubmit}
              style={{ display: "flex", flexDirection: "column" }}
            >
              <TextField
                label="Email"
                variant="outlined"
                name="email"
                type="email"
                style={{
                  marginBottom: "1rem",
                  width: "20rem",
                  backgroundColor: "white",
                }}
                onInput={(e) => setEmail(e.target.value)}
                required
              />
              <TextField
                label="Password"
                variant="outlined"
                type="password"
                name="password"
                style={{
                  marginBottom: "1rem",
                  width: "20rem",
                  backgroundColor: "white",
                }}
                onInput={(e) => setPassword(e.target.value)}
                required
              />
              <Button variant="contained" type="submit">
                Login
              </Button>
            </form>
          </div>
        </>
      )}
    </Layout>
  );
};

export default Login;
