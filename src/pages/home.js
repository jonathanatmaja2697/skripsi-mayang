import {
  emailAtom,
  moduleAnswerTemp,
  moduleCriteriaAtom,
  moduleListAtom,
  trainingModuleResultAtom,
} from "../atoms/atoms";
import { useEffect, useState } from "react";

import { Card } from "@mui/material";
import { Layout } from "../components/layout";
import { Logout } from "../components/logout";
import { LowestModule } from "../components/lowest-module";
import { ModuleQuestions } from "../components/module-questions";
import { calculateResult } from "../helpers/helper";
import dayjs from "dayjs";
import { useAtom } from "jotai";
import { useRouter } from "next/router";

const Home = () => {
  const [email] = useAtom(emailAtom);
  const [moduleCriteria] = useAtom(moduleCriteriaAtom);
  const [moduleList] = useAtom(moduleListAtom);
  const router = useRouter();
  const [moduleAnswer, setModuleAnswer] = useAtom(moduleAnswerTemp);
  const [trainingModuleResult, setTrainingModuleResult] = useAtom(
    trainingModuleResultAtom
  );
  const [showResult, setShowResult] = useState(false);
  const [lowestModule, setLowestModule] = useState("");

  useEffect(() => {
    if (trainingModuleResult.length > 0) {
      const match = trainingModuleResult.find((v) => v.email === email);
      if (typeof match !== "undefined" && match.email) {
        setShowResult(true);
        setLowestModule(match.module);
      }
    }
  }, []);

  const onSubmit = () => {
    if (moduleAnswer.length === moduleCriteria.length * 4) {
      const lowestModule = calculateResult(
        moduleList,
        moduleCriteria,
        moduleAnswer
      );

      setTrainingModuleResult((prev) => [
        ...prev,
        {
          email,
          module: lowestModule,
          date: dayjs().format("DD/MM/YYYY HH:mm:ss"),
          batch: Math.floor((dayjs().month() + 6) / 6),
        },
      ]);
      router.reload();
    } else {
      alert("Please fill all answers");
    }
  };

  return (
    <Layout>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "flex-end",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        <Logout />
        {email === "admin@gmail.com" ? (
          <div
            style={{
              display: "flex",
              flex: 20,
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <Card
              style={{
                padding: "5vh",
                width: "90%",
                textAlign: "center",
                cursor: "pointer",
              }}
              onClick={() => {
                router.push("/module/create");
              }}
            >
              <b>Tambah Modul</b>
            </Card>
            <Card
              style={{
                padding: "5vh",
                marginTop: "5vh",
                marginBottom: "5vh",
                width: "90%",
                textAlign: "center",
                cursor: "pointer",
              }}
              onClick={() => {
                router.push("/module-list");
              }}
            >
              <b>List Modul</b>
            </Card>
            <Card
              style={{
                padding: "5vh",
                width: "90%",
                textAlign: "center",
                cursor: "pointer",
              }}
              onClick={() => {
                router.push("/training-result");
              }}
            >
              <b>Hasil Pengolahan</b>
            </Card>
          </div>
        ) : (
          //user
          <div style={{ flex: 15, paddingLeft: "1rem", paddingRight: "1rem" }}>
            {showResult && <LowestModule lowestModule={lowestModule} />}
            <ModuleQuestions
              moduleCriteria={moduleCriteria}
              onSubmit={onSubmit}
              setModuleAnswer={setModuleAnswer}
              shown={showResult}
            />
          </div>
        )}
      </div>
    </Layout>
  );
};

export default Home;
