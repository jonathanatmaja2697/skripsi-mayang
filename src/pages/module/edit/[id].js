import {
  Button,
  Card,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import {
  moduleCriteriaAtom,
  moduleListAtom,
  scoringNameOne,
} from "../../../atoms/atoms";
import { useEffect, useState } from "react";

import { Layout } from "../../../components/layout";
import { Logout } from "../../../components/logout";
import { useAtom } from "jotai";
import { useRouter } from "next/router";

const EditModule = () => {
  const [moduleList, setModuleList] = useAtom(moduleListAtom);
  const [moduleCriteria, setModuleCriteria] = useAtom(moduleCriteriaAtom);
  const [module, setModule] = useState({});
  const [criteria, setCriteria] = useState({});
  const router = useRouter();
  const { id } = router.query;
  const [showEditForm, setShowEditForm] = useState(false);

  useEffect(() => {
    if (!router.isReady) return;
    const existingModule = moduleList.filter((v) => v.id === id);
    const existingCriteria = moduleCriteria.filter((v) => v.id === id);

    if (existingCriteria?.length > 0 && existingModule?.length > 0) {
      setModule(existingModule[0]);
      setCriteria(existingCriteria[0].criteria);
      setShowEditForm(true);
    }
  }, [router.isReady]);

  const onSubmit = (e) => {
    e.preventDefault();
    const newCriteriaSet = {
      id,
      criteria: [
        {
          id: 1,
          name: e.target?.criteria1.value || "-",
          scoringNameOne,
          scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
          weight: 0.4,
        },
        {
          id: 2,
          name: e.target?.criteria2.value || "-",
          scoringNameOne,
          scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
          weight: 0.3,
        },
        {
          id: 3,
          name: e.target?.criteria3.value || "-",
          scoringNameOne,
          scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
          weight: 0.1,
        },
        {
          id: 4,
          name: e.target?.criteria4.value || "-",
          scoringNameOne,
          scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
          weight: 0.2,
        },
      ],
    };

    setModuleCriteria(
      moduleCriteria.map((v) => (v.id === id ? newCriteriaSet : v))
    );

    if (e.target?.moduleName.value !== module.name) {
      const newModuleSet = {
        id,
        name: e.target?.moduleName.value,
      };

      setModuleList(moduleList.map((v) => (v.id === id ? newModuleSet : v)));
    }
    router.push("/module-list");
  };

  // const onSubmit = (e) => {
  //   e.preventDefault();
  //   const moduleName = e.target?.moduleName?.value || "-";

  //   const lastId = moduleList[moduleList.length - 1].id.replace(
  //     parseInt(moduleList[moduleList.length - 1].id),
  //     parseInt(moduleList[moduleList.length - 1].id) + 1
  //   );

  //   //check duplicate module
  //   const duplicate = moduleList.some((v) => v.name === moduleName);
  //   const duplicateCriteria = moduleCriteria.some((v) => v.id === lastId);

  //   if (duplicate || duplicateCriteria) {
  //     alert("Modul sudah terdaftar");
  //   } else {
  //     const newModule = {
  //       id: lastId,
  //       name: moduleName,
  //     };

  //     const newCriteria = {
  //       id: lastId,
  //       criteria: [
  //         {
  //           id: 1,
  //           name: e.target.criteria1.value,
  //           scoringNameOne,
  //           scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
  //           weight: 0.4,
  //         },
  //         {
  //           id: 2,
  //           name: e.target.criteria2.value,
  //           scoringNameOne,
  //           scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
  //           weight: 0.3,
  //         },
  //         {
  //           id: 3,
  //           name: e.target.criteria3.value,
  //           scoringNameOne,
  //           scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
  //           weight: 0.1,
  //         },
  //         {
  //           id: 4,
  //           name: e.target.criteria4.value,
  //           scoringNameOne,
  //           scoring: [0.1, 0.2, 0.3, 0.7, 0.8],
  //           weight: 0.2,
  //         },
  //       ],
  //     };

  //     setModuleList((prev) => [...prev, newModule]);
  //     setModuleCriteria((prev) => [...prev, newCriteria]);
  //     router.push("/module-list");
  //   }
  // };
  return (
    <Layout>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "flex-end",
          flexDirection: "column",
          minHeight: "100vh",
          padding: "1rem",
        }}
      >
        <Logout />
        <div style={{ flex: 20 }}>
          {showEditForm ? (
            <>
              <h2>Tambah Modul</h2>
              <form onSubmit={onSubmit}>
                <TextField
                  placeholder="Nama modul"
                  style={{
                    color: "white",
                    backgroundColor: "white",
                    marginBottom: "2rem",
                  }}
                  variant="outlined"
                  size="small"
                  required
                  name="moduleName"
                  defaultValue={module.name}
                />
                <Card style={{ padding: "2rem" }}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    <b>Skala penilaian & bobot yang berlaku</b>
                    <TableContainer
                      component={Paper}
                      style={{ marginBottom: "2rem", marginTop: "1rem" }}
                    >
                      <Table>
                        <TableHead>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              <b>Skala Penilaian</b>
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>
                              <b>Bobot</b>
                            </TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              Tidak
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>0.1</TableCell>
                          </TableRow>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              Kurang
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>0.2</TableCell>
                          </TableRow>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              Cukup
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>0.3</TableCell>
                          </TableRow>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              Jelas
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>0.7</TableCell>
                          </TableRow>
                          <TableRow style={{ flexGrow: 1 }}>
                            <TableCell style={{ width: "25%" }}>
                              Sangat
                            </TableCell>
                            <TableCell style={{ width: "25%" }}>0.8</TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </TableContainer>
                    <b>Kriteria</b>

                    <ol>
                      <li>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                          }}
                        >
                          <input
                            placeholder="Kriteria pertanyaan 1"
                            required
                            type="text"
                            style={{ width: "15rem", padding: "1%" }}
                            name="criteria1"
                            defaultValue={criteria[0].name}
                          />
                          <span style={{ fontSize: "12px" }}>
                            Bobot: <b>0.4</b>
                          </span>
                        </div>
                      </li>
                      <li>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                          }}
                        >
                          <input
                            placeholder="Kriteria pertanyaan 2"
                            required
                            type="text"
                            style={{ width: "15rem", padding: "1%" }}
                            name="criteria2"
                            defaultValue={criteria[1].name}
                          />
                          <span style={{ fontSize: "12px" }}>
                            Bobot: <b>0.3</b>
                          </span>
                        </div>
                      </li>
                      <li>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                          }}
                        >
                          <input
                            placeholder="Kriteria pertanyaan 3"
                            required
                            type="text"
                            style={{ width: "15rem", padding: "1%" }}
                            name="criteria3"
                            defaultValue={criteria[2].name}
                          />
                          <span style={{ fontSize: "12px" }}>
                            Bobot: <b>0.1</b>
                          </span>
                        </div>
                      </li>
                      <li>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "flex-start",
                          }}
                        >
                          <input
                            placeholder="Kriteria pertanyaan 4"
                            required
                            type="text"
                            style={{ width: "15rem", padding: "1%" }}
                            name="criteria4"
                            defaultValue={criteria[3].name}
                          />
                          <span style={{ fontSize: "12px" }}>
                            Bobot: <b>0.2</b>
                          </span>
                        </div>
                      </li>
                    </ol>
                    <Button type="submit" variant="contained">
                      Submit
                    </Button>
                  </div>
                </Card>
              </form>{" "}
            </>
          ) : (
            <div style={{ textAlign: "center" }}>
              <h3>Modul tidak ditemukan</h3>
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default EditModule;
