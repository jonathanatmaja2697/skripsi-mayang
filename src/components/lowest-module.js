import { Card } from "@mui/material";
import React from "react";

export const LowestModule = ({ lowestModule = "Test" }) => {
  return (
    <Card
      style={{
        width: "50%",
        display: "block",
        marginRight: "auto",
        marginLeft: "auto",
        textAlign: 'center'
      }}
    >
      <p>Modul yang butuh training adalah</p>
      <h1>{lowestModule}</h1>
    </Card>
  );
};
