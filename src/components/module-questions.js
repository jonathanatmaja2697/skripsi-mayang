import { Button } from "@mui/material";
import { getModuleNameById } from "../helpers/helper";
import { ModuleCard } from "./module-card";

export const ModuleQuestions = ({
  moduleCriteria,
  shown,
  setModuleAnswer,
  onSubmit,
}) => {
  if (moduleCriteria.length > 0 && !shown) {

    return moduleCriteria.map((v, i) => {
      return (
        <div key={i}>
          <ModuleCard
            moduleName={getModuleNameById(v.id)}
            moduleId={v.id}
            data={v.criteria}
            setAnswer={setModuleAnswer}
          />
          {i + 1 === moduleCriteria.length && (
            <Button
              variant="contained"
              style={{
                textTransform: "none",
                marginTop: "1rem",
              }}
              onClick={onSubmit}
            >
              Submit
            </Button>
          )}
        </div>
      );
    });
  } else {
    return <div></div>;
  }
};
