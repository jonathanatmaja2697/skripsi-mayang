import { useEffect, useState } from "react";

import { Provider } from "jotai";
import { emailAtom } from "../atoms/atoms";
import { useAtomValue } from "jotai/utils";
import { useRouter } from "next/router";

export const Layout = ({ children, centered = false }) => {
  const [showing, setShowing] = useState(false);
  const email = useAtomValue(emailAtom);
  const router = useRouter();
  useEffect(() => {
    setShowing(true);
    if (email === "") {
      router.push("/login");
    }
  }, []);

  if (!showing) {
    return null;
  }

  if (typeof window === "undefined") {
    return <></>;
  } else {
    if (
      email !== "" ||
      window.location.pathname === "/login" ||
      typeof email !== "undefined"
    ) {
      return (
        <Provider>
          <div
            suppressHydrationWarning
            style={{
              flex: 1,
              display: "flex",
              height: "100%",
              justifyContent: "center",
              alignItems: centered ? "center" : "flex-start",
              minHeight: "100vh",
              marginRight: "auto",
              marginLeft: "auto",
              maxWidth: "80%",
              backgroundColor: "lightgrey",
            }}
          >
            {children}
          </div>
        </Provider>
      );
    }
  }
};
