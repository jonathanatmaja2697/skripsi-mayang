import React from "react";

export const ModuleCard = ({ moduleName, moduleId, data, setAnswer }) => {
  const onChange = (e, i) => {
    setAnswer((prev) => {
      if (prev.length > 0) {
        const indexOfObject = prev.findIndex((obj) => {
          return obj.criteria === i && obj.moduleId === moduleId;
        });
        if (indexOfObject >= 0) {
          prev.splice(indexOfObject, 1);
          prev.push({ moduleId, criteria: i, weight: e.target.value });
          return prev;
        } else {
          return [...prev, { moduleId, criteria: i, weight: e.target.value }];
        }
      } else {
        return [{ moduleId, criteria: i, weight: e.target.value }];
      }
    });
  };

  return (
    <div>
      <h5>{moduleName}</h5>
      <ol>
        {data.map((v, i) => {
          console.log(v.scoringName.map((v) => console.log(v)));
          return (
            <li key={i}>
              <select onChange={(e) => onChange(e, i)} defaultValue="DEFAULT">
                <option value="DEFAULT" disabled>
                  -- select an option --
                </option>
                {v?.scoringName.map((s, i) => {
                  return (
                    <option key={i} value={v.scoring[i]}>
                      {s}
                    </option>
                  );
                })}
              </select>
              &nbsp;
              {v.name}
            </li>
          );
        })}
      </ol>
    </div>
  );
};
