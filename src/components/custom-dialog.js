import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

import React from "react";

export const CustomDialog = ({
  dialogTitle = "",
  dialogContent,
  open,
  onClose,
  onAgree,
}) => {
  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{dialogTitle}</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ textAlign: "center" }}>
            {dialogContent}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Batal</Button>
          <Button onClick={onAgree}>Lanjutkan</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
