import React from "react";
import { useRouter } from "next/router";
import { useAtom } from "jotai";
import { emailAtom } from "../atoms/atoms";

export const Logout = () => {
  const router = useRouter();
  const [, setEmail] = useAtom(emailAtom);

  const onLogout = () => {
    setEmail("");
    router.push("/login");
  };

  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        paddingRight: "4vh",
      }}
    >
      <h3
        onClick={() => {
          onLogout();
        }}
        style={{ cursor: "pointer" }}
      >
        Logout
      </h3>
    </div>
  );
};
